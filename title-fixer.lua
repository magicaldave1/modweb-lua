-- local exclude_paths = config["exclude_paths"]
local exclude_paths = global_data.global_exclude_paths

if not global_data.mod_titles then
  global_data.mod_titles = {}
end

if not exclude_paths then
  Plugin.fail('[TITLE FIXER]: Exclude paths are mandatory!')
end

local content = HTML.select_one(page, 'div#content')
local header = HTML.select_any_of(page, { 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' })

local modData = HTML.select_one(page, 'modData')

-- The modData element is applied in the previous stage, mod-paths
-- If it's not found, the previous stage failed somehow
if not modData then
  Plugin.fail('[TITLE FIXER]: Mandatory element was not found in the page! modData missing!')
end

modPath = HTML.get_attribute(modData, 'path')

function getSeparator()
  if Sys.is_windows() then
    return '\\'
  else
    return '/'
  end
end

do_log = global_data.do_log
function doLog(level, entry)
  if not do_log or do_log ~= "true" then return end

  if level == "debug" then
    Log.debug(entry)
  elseif level == "error" then
    Log.error(entry)
  elseif level == "info" then
    Log.info(entry)
  elseif level == "warning" then
    Log.warning(entry)
  end
end

--- Takes a mod title and inserts it as the first body element
--- Also adds a link to the mod page, including the changelog
function prependLink(titleText)

  if (titleText == nil) then
    Plugin.fail('[TITLE FIXER]: Supplied title text is nil!')
  end

  local new_link = page_url

  new_link = Regex.replace(new_link, 'changelog', '')

  if not String.ends_with(new_link, '/') then
    new_link = new_link .. '/'
  end

  local link = HTML.create_element('a', titleText)
  HTML.add_class(link, 'modHeader')
  HTML.set_attribute(link, 'href', new_link)
  HTML.set_attribute(link, 'id', modPath)

  -- I can't and won't help you if there is no page body.
  local body = HTML.select_one(page, 'body')
  HTML.prepend_child(body, link)
end

-- Exclude paths provided in the initial stage
-- Used here to omit certain directories from the title fixing process
if Regex.match(Sys.dirname_url(target_file), exclude_paths) then
  doLog('debug', "[TITLE FIXER]: Skipping title fixer for excluded page: " .. target_file)
  return
end

if content then
  doLog('info', "[TITLE FIXER]: Found content div for file: " .. target_file)

  if Regex.match(target_file, 'changelog') then
    local changelog_dir = Sys.dirname(target_file)
    doLog('info', "[TITLE FIXER]: This thing is a changelog! : " .. changelog_dir)

    local changelog_parent = changelog_dir .. getSeparator() .. '..' .. getSeparator() .. 'index.html'
    doLog('info', "[TITLE FIXER]: Checking for parent index.html file: " .. changelog_parent)

    if Sys.is_file(changelog_parent) then
      doLog('info', "[TITLE FIXER]: Found a parent index.html file for the changelog! : " .. changelog_parent)

      local parent_page = Sys.read_file(changelog_parent)
      local parent_HTML = HTML.parse(parent_page)
      local parent_modData = HTML.select_one(parent_HTML, '.modHeader')

      if parent_modData then
        local parent_title = HTML.inner_text(parent_modData)
        doLog('info', '[TITLE FIXER]: Found modHeader element in the parent page! ' .. parent_title)

        prependLink(parent_title)
      else
        Plugin.fail('[TITLE FIXER]: Mandatory element was not found in the parent page! modHeader missing!')
      end
    else
      doLog('info', "[TITLE FIXER]: No parent index.html file found for the changelog! WE NEED TO GO DEEPER : " .. changelog_parent)

      local modPath = HTML.get_attribute(modData, 'path')

      if not modPath then
        Plugin.fail('[TITLE FIXER]: Mandatory attribute was not found in the changelog! modPath missing!')
      end

      doLog('debug', '[TITLE FIXER]: page url is: ' .. page_url)

      local changelog_parent = changelog_dir
        .. getSeparator() .. '..'
        .. getSeparator() .. '..'

      -- Mod changelogs are nested in $MOD_NAME/changelog,
      -- whereas the root changelog is in /changelog
      -- So in order to escape, *we must go deeper*
      if page_url ~= '/changelog' then
        changelog_parent = changelog_parent .. getSeparator() .. '..'
      end

      -- Regardless of whether it's a mod or not, the final parent is always going to be in /site
      changelog_parent = changelog_parent .. getSeparator() .. 'site'

      -- If it's a mod, we need to go deeper
      if page_url ~= '/changelog' then
        changelog_parent = changelog_parent .. getSeparator() .. modPath
      end

      -- Finally we arrive at the expected result for either case
      changelog_parent = changelog_parent .. getSeparator() .. 'index.md'

      doLog('debug', "[TITLE FIXER]: Checking for parent index.md file: " .. changelog_parent)

      if Sys.is_file(changelog_parent) then
        doLog('debug', "[TITLE FIXER]: Found a parent index.md file for the changelog! : " .. changelog_parent)

        local parent_page = Sys.read_file(changelog_parent)

        local parent_strings = Regex.split(parent_page, '\n')
        local parent_length = Table.length(parent_strings)
        local parent_title = String.trim(Regex.replace_all(parent_strings[1], '#', ''))
        doLog('debug', '[TITLE FIXER]: Guessed parent title: ' .. parent_title)
        prependLink(parent_title)
      end
    end
      return
  end

  if header then
    local title = HTML.inner_text(header)

    doLog('warning', "[TITLE FIXER]: Located page title: " .. title .. " for file: " .. target_file)

    if page_url ~= '/' then
      Log.warning("[TITLE FIXER]: Adding mod title to global data: " .. title)
      local modLength = Table.length(global_data.mod_titles) + 1
      global_data.mod_titles[modLength] = {mod_title = title, mod_path = modPath}
    end

    HTML.delete(header)

    prependLink(title)

    -- Prepends the dev build warning to non-changelog pages
    local content = HTML.select_one(page, 'div#content')
    local dev_message = config["dev_message"] or "Releases without a download link can be downloaded as a dev build link above."
    if config['dev_warning'] and config['dev_warning'] == 'true' then
      local releaseMsg = HTML.create_element('p', dev_message)
      HTML.prepend_child(content, releaseMsg)
    end
  else
    Plugin.fail('[TITLE FIXER]: No title found in the page! ' .. target_file)
  end
end

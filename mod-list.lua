global_exclude_paths = global_data.global_exclude_paths

if not global_exclude_paths then
    Plugin.fail("[MOD LIST]: Exclude paths are mandatory!")
end

do_log = global_data.do_log
function doLog(level, entry)
  if not do_log or do_log ~= "true" then return end

  if level == "warning" then
    Log.warning(entry)
  elseif level == "error" then
    Log.error(entry)
  elseif level == "debug" then
    Log.debug(entry)
  elseif level == "info" then
    Log.info(entry)
  end
end

function middleIndex(left, right)
return left + ((right - left) - (right - left) % 2) / 2
end

function merge(arr, left, mid, right)
  local n1 = mid - left + 1
  local n2 = right - mid
  local L, R = {}, {}
  local i, j, k

  i = 1
  while i <= n1 do
    L[i] = arr[left + i - 1]
    i = i + 1
  end

  j = 1
  while j <= n2 do
    R[j] = arr[mid + j]
    j = j + 1
  end

  i = 1
  j = 1
  k = left
  while i <= n1 and j <= n2 do
    if L[i].mod_path <= R[j].mod_path then
      arr[k] = L[i]
      i = i + 1
    else
      arr[k] = R[j]
      j = j + 1
    end
    k = k + 1
  end

  while i <= n1 do
    arr[k] = L[i]
    i = i + 1
    k = k + 1
  end

  while j <= n2 do
    arr[k] = R[j]
    j = j + 1
    k = k + 1
  end
end

function mergeSort(arr, left, right)
  if left < right then
    local mid = middleIndex(left, right)
    mergeSort(arr, left, mid)
    mergeSort(arr, mid + 1, right)

    merge(arr, left, mid, right)
  end
end

function getSeparator()
  if Sys.is_windows() then
    return '\\'
  else
    return '/'
  end
end

function skipDir(dir)
  local dirSplit = Regex.split(dir, getSeparator())
  local baseDir = dirSplit[Table.length(dirSplit)]
  return Regex.match(baseDir, global_exclude_paths)
end

if not global_data.multi_mod then
    Log.warning("[MOD LIST]: Not in a multi-mod configuration. Skipping mod list generation.")
    return
end

local mod_dirs = global_data.mod_dirs
local mod_titles = global_data.mod_titles
local numDirs = Table.length(mod_dirs)
local numTitles = Table.length(mod_titles)

if not mod_titles then
    Plugin.fail("[MOD LIST]: No mod titles found!")
end

if numTitles ~= numDirs then
    Plugin.fail("[MOD LIST]: Number of mod titles and mod directories do not match!")
end

local num_mods = Table.length(mod_titles)
local modN = num_mods

mergeSort(mod_titles, 1, num_mods)

local root_page = build_dir .. getSeparator() .. "index.html"
local page = HTML.parse(Sys.read_file(root_page))
local mod_marker = HTML.select_one(page, "#modMarker")

if not mod_marker then
    Log.warning("[MOD LIST]: No mod marker found! Can't do anything.")
    return
end

local prod_mode = 'false'

if Sys.is_file('.prod') then
    prod_mode = 'true'
end

while modN >= 1 do
    local mod = mod_titles[modN]
    local mod_title = mod["mod_title"]
    local mod_path = mod["mod_path"]

    Log.info("[MOD LIST]: Mod " .. modN .. ": " .. mod_title .. " at " .. mod_path)

    local mod_link_container = HTML.create_element("div")
    HTML.set_attribute(mod_link_container, "class", "modLinkContainer")

    local mod_link = HTML.create_element("a", mod_title)
    HTML.set_attribute(mod_link, "id", mod_path)

    if prod_mode == 'true' then
        mod_path = global_data.project .. '/' .. mod_path
    end

    HTML.set_attribute(mod_link, "href", '/' .. mod_path .. '/')
    HTML.add_class(mod_link, "modHeader")
    HTML.append_child(mod_link_container, mod_link)

    HTML.insert_after(mod_marker, mod_link_container)

    modN = modN - 1
end

HTML.delete(mod_marker)

Sys.write_file(root_page, HTML.pretty_print(page))

function deleteOldPackages()

    local siteDirs = global_data.mod_dirs
    local modN = 1
    local numDirs = Table.length(siteDirs)

    while modN <= numDirs do
        local modDir = siteDirs[modN]

        doLog('warning', "[MOD LIST]: Deleting old site directory: " .. modDir)

        if Sys.is_dir(modDir) and not skipDir(modDir) then
            Sys.delete_recursive(modDir)
        end

        modN = modN + 1
    end

    if Sys.is_file('site' .. getSeparator() .. 'index.md') then
        doLog('warning', "[MOD LIST]: Deleting old site index.md")
        Sys.delete_file('site' .. getSeparator() .. 'index.md')
    end

    if Sys.is_file('site' .. getSeparator() .. 'changelog.md') then
        doLog('warning', "[MOD LIST]: Deleting old site changelog.md")
        Sys.delete_file('site' .. getSeparator() .. 'changelog.md')
    end

    if Sys.is_file('.pkg') then
        Sys.delete_file('.pkg')
    end

    if Sys.is_file('.prod') then
        Sys.delete_file('.prod')
    end

end

deleteOldPackages()

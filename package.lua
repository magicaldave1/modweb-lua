global_exclude_paths = global_data.global_exclude_paths
use_digests = global_data.use_digests

if not global_exclude_paths then
  Plugin.fail('[PACKAGER]: Exclude paths are mandatory!')
end

do_log = global_data.do_log
function doLog(level, entry)
  if not do_log or do_log ~= "true" then return end

  if level == "warning" then
    Log.warning(entry)
  elseif level == "error" then
    Log.error(entry)
  elseif level == "debug" then
    Log.debug(entry)
  elseif level == "info" then
    Log.info(entry)
  end
end

function getSeparator()
  if Sys.is_windows() then
    return '\\'
  else
    return '/'
  end
end

function skipDir(dir)
  local dirSplit = Regex.split(dir, getSeparator())
  local baseDir = dirSplit[Table.length(dirSplit)]
  return Regex.match(baseDir, global_exclude_paths)
end

package_command = ""
if not Sys.is_windows() then
    package_command = "cd ../MOD_PATH && zip -T ../MOD_PATH.zip * && cd ../web"
else
  Plugin.fail("[PACKAGER]: Windows is not supported yet. Or maybe it is?")
  -- Change to the directory containing the directory to be zipped
  package_command = 'cd /d "..\\MOD_PATH"'

  -- Create the zip file
  package_command = package_command .. ' && ' .. 'zip ..\\"MOD_PATH".zip *'

  -- Change back to the website directory
  package_command = package_command .. ' && ' .. 'cd /d ..\\web'
end

if not Sys.file_exists('.pkg') then
  Sys.write_file('.pkg', '')
  doLog("warning", "[PACKAGER]: Package profile enabled, mods will be deployed during this run.")

  local possible_mod_dirs = Sys.list_dir('..')

  local modN = 1
  local modDirs = Table.length(possible_mod_dirs)
  local modsPackaged = 0

  while modN <= modDirs do
    local modDir = possible_mod_dirs[modN]

    if skipDir(modDir) or Sys.is_file(modDir) then
      doLog('warning', "[PACKAGER]: Skipping packaging for: " .. modDir)
    else
      local separator = getSeparator()
      local dirSplit = Regex.split(modDir, separator)
      local baseDir = dirSplit[Table.length(dirSplit)]
      baseDir = Regex.replace_all(baseDir, '-', '_')

      local command = Regex.replace_all(package_command, 'MOD_PATH', baseDir)

      local output_file = '..' .. separator .. baseDir .. '.zip'

      if Sys.file_exists(output_file) then
        Sys.delete_file(output_file)
      end

      Sys.run_program(command)

      if use_digests and use_digests == "true" then

        local blake_digest = Digest.blake2s(Sys.read_file(output_file))
        local sha256_digest = Digest.sha256(Sys.read_file(output_file))
        local sha512_digest = Digest.sha512(Sys.read_file(output_file))

        Sys.write_file(output_file .. '.blake2s.txt', blake_digest)
        Sys.write_file(output_file .. '.sha256.txt', sha256_digest)
        Sys.write_file(output_file .. '.sha512.txt', sha512_digest)

      end

      doLog('warning', "[PACKAGER]: Packaged mod: " .. baseDir)
      modsPackaged = modsPackaged + 1
      -- global_data.mod_dirs[modDir] = modDir
    end

    modN = modN + 1
  end

  local final_package_command = 'cd .. && zip -T PROJECT_NAME.zip *.zip README.md CHANGELOG.md && cd web'
  local clean_project_name = Regex.replace_all(global_data.project, '-', '_')
  final_package_command = Regex.replace(final_package_command, 'PROJECT_NAME', clean_project_name)

  Sys.run_program(final_package_command)

  local output_file = '..' .. getSeparator() .. clean_project_name .. '.zip'

  if use_digests and use_digests == 'true' then

    local blake_digest = Digest.blake2s(Sys.read_file(output_file))
    local sha256_digest = Digest.sha256(Sys.read_file(output_file))
    local sha512_digest = Digest.sha512(Sys.read_file(output_file))

    Sys.write_file(output_file .. '.blake2s.txt', blake_digest)
    Sys.write_file(output_file .. '.sha256.txt', sha256_digest)
    Sys.write_file(output_file .. '.sha512.txt', sha512_digest)

  end

  doLog('warning', "[PACKAGER]: Packaged " .. modsPackaged .. " mods.")

else
  return
end

# modweb-lua

This project provides Lua plugins for soupault.

## Usage

1. Add this repo as a submodule to your own:

        # Change `web/plugins` to match the desired path to your plugins
        git submodule add https://gitlab.com/modding-openmw/modweb-lua web/plugins

1. Add widgets to your site config (see below)

## Widgets

### prod-mode

Turn `<a>` and `<link>` `href`s as well as `<img>` and `<script>` `src`es into versions that will play nicely with how Gitlab's (and maybe also others') static site hosting works.

To use, add the following to your soupault config file:

    [widgets.prod-mode]
      widget = "prod-mode"
      project = "your-mod-repo-slug-here"

And include this script in your project's plugins directory. Replace `your-mod-repo-slug-here` with the slug of your mod's Gitlab repo.

function doLog(level, entry)
  if not do_log or do_log ~= "true" then return end

  if level == "warning" then
    Log.warning(entry)
  elseif level == "error" then
    Log.error(entry)
  elseif level == "debug" then
    Log.debug(entry)
  elseif level == "info" then
    Log.info(entry)
  end
end

function getSeparator()
  if Sys.is_windows() then
    return '\\'
  else
    return '/'
  end
end

do_log = config["do_log"]
global_exclude_paths = config['global_exclude_paths']
multi_mod = config["multi_mod"]
local org = config["org"]
local project = config["project"]
local use_digests = config["use_digests"]

if not org or not project or not global_exclude_paths then
  Plugin.fail("Org, Project, and Global Exclude Paths are all mandatory!")
end

doLog('info', "[MOD-INFO]: Org: " .. org)
doLog('info', "[MOD-INFO]: Project: " .. project)
doLog('info', "[MOD-INFO]: Should log: " .. do_log)
doLog('info', "[MOD-INFO]: Global exclude paths: " .. global_exclude_paths)

global_data.discord = config["contact_discord"]
global_data.issues_page = config["contact_issues_page"]
global_data.irc = config["contact_irc"]
global_data.email = config["contact_email"]

global_data.doLog = doLog
global_data.do_log = do_log
global_data.global_exclude_paths = global_exclude_paths
global_data.multi_mod = multi_mod
global_data.org = org
global_data.project = project
global_data.use_digests = use_digests
global_data.mod_dirs = {}

function skipDir(dir)
  local dirSplit = Regex.split(dir, getSeparator())
  local baseDir = dirSplit[Table.length(dirSplit)]
  return Regex.match(baseDir, global_exclude_paths)
end

function deleteOldPackages()
  local possible_mod_pkgs = Sys.list_dir('..')
  local modPkgs = Table.length(possible_mod_pkgs)
  local modN = 1

  while modN <= modPkgs do
    local modPkg = possible_mod_pkgs[modN]

    if Sys.is_file(modPkg) and Regex.match(modPkg, '.zip') then
      Sys.delete_file(modPkg)

      if Sys.is_file(modPkg .. 'sha256.txt') then
        Sys.delete_file(modPkg .. 'sha256.txt')
      end

      if Sys.is_file(modPkg .. 'sha512.txt') then
        Sys.delete_file(modPkg .. 'sha512.txt')
      end

      if Sys.is_file(modPkg .. 'blake2s.txt') then
        Sys.delete_file(modPkg .. 'blake2s.txt')
      end

    end

    modN = modN + 1
  end
end

function copyModDocs()

  if (multi_mod and multi_mod == 'true') then
    Log.warning("[BUILDER]: Multi mod detected, copying mod READMEs and CHANGELOGs")
    local possible_mod_dirs = Sys.list_dir('..')
    local modDirs = Table.length(possible_mod_dirs)
    local modN = 1

    while modN <= modDirs do
      local modDir = possible_mod_dirs[modN]

      if Sys.is_dir(modDir) and not skipDir(modDir) then
        doLog('debug', "[BUILDER]: Current modDir: " .. modDir)

        local readmePath = modDir .. getSeparator() .. "README.md"
        local changelogPath = modDir .. getSeparator() .. "CHANGELOG.md"

        if Sys.is_file(readmePath) then

          doLog('debug', "[BUILDER]: Copying README.md from " .. modDir .. " to site")
          local readme = Sys.read_file(readmePath)
          local fullDir = Regex.split(modDir, getSeparator())
          local baseDir = fullDir[Table.length(fullDir)]

          if not Sys.is_dir("site" .. getSeparator() .. baseDir) then
            Sys.mkdir("site" .. getSeparator() .. baseDir)
          end

          Sys.write_file("site" .. getSeparator() .. baseDir .. getSeparator() .. "index.md", readme)
        end

        if Sys.is_file(changelogPath) then

          doLog('debug', "[BUILDER]: Copying CHANGELOG.md from " .. modDir .. " to site")
          local changelog = Sys.read_file(changelogPath)
          local fullDir = Regex.split(modDir, getSeparator())
          local baseDir = fullDir[Table.length(fullDir)]

          if not Sys.is_dir("site" .. getSeparator() .. baseDir) then
            Sys.mkdir("site" .. getSeparator() .. baseDir)
          end

          Sys.write_file("site" .. getSeparator() .. baseDir .. getSeparator() .. "changelog.md", changelog)
        elseif not skipDir(changelogPath) then
          local realFakeChangelog = HTML.create_element("div")
          HTML.set_attribute(realFakeChangelog, "id", "changeContainer")

          local changelogTempl = HTML.create_element("h1", "Changelog")
          local changelogContent = HTML.create_element("p", "No changelog available for this mod.")

          HTML.append_child(realFakeChangelog, changelogTempl)
          HTML.append_child(realFakeChangelog, changelogContent)

          local fullDir = Regex.split(modDir, getSeparator())
          local baseDir = fullDir[Table.length(fullDir)]

          if not Sys.is_dir("site" .. getSeparator() .. baseDir) then
            Sys.mkdir("site" .. getSeparator() .. baseDir)
          end

          Sys.write_file("site" .. getSeparator() .. baseDir .. getSeparator() .. "changelog.md" , HTML.pretty_print(realFakeChangelog))
        end
      end

      modN = modN + 1
    end

  end

  if Sys.is_file(".." .. getSeparator() .. "README.md") then
    doLog('debug', "[BUILDER]: Copying root README.md")
    local readme = Sys.read_file(".." .. getSeparator() .. "README.md")
    Sys.write_file("site" .. getSeparator() .. "index.md", readme)
  else
    Plugin.fail("[BUILDER]: No README.md found in root directory!")
  end

  if Sys.is_file(".." .. getSeparator() .. "CHANGELOG.md") then
    doLog('debug', "[BUILDER]: Copying root CHANGELOG.md")
    local readme = Sys.read_file(".." .. getSeparator() .. "CHANGELOG.md")
    Sys.write_file("site" .. getSeparator() .. "changelog.md", readme)
  else

    local realFakeChangelog = HTML.create_element("div")
    HTML.set_attribute(realFakeChangelog, "id", "changeContainer")

    local changelogTempl = HTML.create_element("h1", "Changelog")
    local changelogContent = HTML.create_element("p", "No changelog available for this mod.")

    HTML.append_child(realFakeChangelog, changelogTempl)
    HTML.append_child(realFakeChangelog, changelogContent)

    Sys.write_file("site" .. getSeparator() .. "changelog.md" , HTML.pretty_print(realFakeChangelog))

  end

end

function prepEnvironment()

  if Sys.file_exists('.pkg') then
    Sys.delete_file('.pkg')
  end

  if Sys.file_exists('.prod') then
    Sys.delete_file('.prod')
  end

  if Sys.is_dir('site') then
    doLog('warning', "[BUILDER]: Site directory exists, removing its contents!")

    local siteDirs = Sys.list_dir('site')
    local numDirs = size(siteDirs)
    local dirN = 1

    while dirN <= numDirs do
      local dir = siteDirs[dirN]
      if Sys.is_dir(dir) and not skipDir(dir) then
          Sys.delete_recursive(dir)
      end
      dirN = dirN + 1
    end
  end

  if Sys.is_dir('build') then
    doLog('warning', "[BUILDER]: Build directory exists, removing its contents!")
    Sys.delete_recursive('build')
  end

  deleteOldPackages()

  copyModDocs()
end

function collectModDirs()
  local dirN = 1
  local dirs = Sys.list_dir("site")
  local dirCount = size(dirs)

  while dirN <= dirCount do
    cwd = dirs[dirN]

    local skip = (skipDir(cwd)
                  or cwd == "site" .. getSeparator() .. "changelog"
                  or cwd == "site" .. getSeparator() .. "blog"
                  or Sys.is_file(cwd))

    if not skip then
      doLog('debug', "[BUILDER]: Detected valid mod directory: " .. cwd)

      local hasMod = 'false'
      local modN = 1
      local modCount = size(global_data.mod_dirs)

      while modN <= modCount do

        if global_data.mod_dirs[modN] == cwd then
          hasMod = 'true'
        end

        modN = modN + 1

      end

      if hasMod ~= 'true' then
        global_data.mod_dirs[Table.length(global_data.mod_dirs) + 1] = cwd
        doLog('info', "[BUILDER]: Added " .. cwd .. " to global mod data paths")
      end

    end

    dirN = dirN + 1
  end

  if global_data.mod_dirs then
    doLog('debug', "[BUILDER]: Mod path count: " .. Table.length(global_data.mod_dirs))
  end

end

prepEnvironment()

collectModDirs()

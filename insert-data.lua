-- insert-data.lua
-- Dave Corley, 2024

check_selector = config["check-selector"]
org = global_data["org"]
project = global_data["project"]
selector = config["selector"]

global_data.mod_paths = {}

if not project or not org then
  Plugin.fail("[DATA BUILDER]: Missing project or org!")
end

dirs = Sys.list_dir('build')

dirCount = size(dirs)
dirN = 1

do_log = global_data.do_log
function doLog(level, entry)
  if not do_log or do_log ~= "true" then return end

  if level == "warning" then
    Log.warning(entry)
  elseif level == "error" then
    Log.error(entry)
  elseif level == "debug" then
    Log.debug(entry)
  elseif level == "info" then
    Log.info(entry)
  end
end

function getSeparator()
  if Sys.is_windows() then
    return '\\'
  else
    return '/'
  end
end

function skipDir(dir)
  local dirSplit = Regex.split(dir, getSeparator())
  local baseDir = dirSplit[Table.length(dirSplit)]
  return Regex.match(baseDir, global_data.global_exclude_paths)
end

function setPathAttr(input_name, field, override_root)
  if not input_name then
    Plugin.fail("[DATA BUILDER]: No input name provided! Mod Path will not be overridden!")
  end

  local elem = HTML.select_one(page, check_selector)

  if not elem then
    Plugin.fail("[DATA BUILDER]: There is not an " .. elem .. "element to write out ")
  end

  doLog('info', "[DATA BUILDER]: Target dir is: " .. Sys.dirname(target_file))

  local file_dirs = Regex.split(Sys.dirname(target_file), getSeparator())
  local baseDir = file_dirs[size(file_dirs)]

  if baseDir == 'changelog' then
    baseDir = file_dirs[size(file_dirs) - 1]
  end

  doLog('info', "[DATA BUILDER]: Comparing base directory: " .. baseDir .. " to input_name " .. input_name)

  if override_root and override_root == 'true' then
    doLog('debug', "[DATA BUILDER]: Overriding root mod data for " .. input_name)
  else
    doLog('debug', "[DATA BUILDER]: Using local info for mod data")
  end

  if not elem then
    Plugin.fail('[DATA BUILDER]: no matching element')
  end

  if elem then

    local target = HTML.select_one(page, selector)

    if not target then
      Plugin.fail("[DATA BUILDER]: page has no element matching selector " .. selector)
      return
    end

    if input_name == baseDir then
      doLog('info', "[DATA BUILDER]: Setting path attr for mod: " .. input_name)
      HTML.set_attribute(target, field, input_name)
      return true
    end

    if (override_root and override_root == 'true') then

      if (field ~= "org" and field ~= "project") then
        doLog('warning', "[DATA BUILDER]: Page URL is being overridden, ensure " .. target_file .. " is a proper root.")
      end

      HTML.set_attribute(target, field, input_name)
    end

  end
end

doLog('info', "[DATA BUILDER]: Mod-path insertion requested for: " .. target_file)

if (not selector) or (not check_selector) then
  doLog('error', "[DATA BUILDER]: selector and check_selector options must be configured")
  return
end

setPathAttr(org, 'org', 'true')
setPathAttr(project, 'project', 'true')

local isRoot = (target_file == "build" .. getSeparator() .. "index.html")
local isRootChangelog = (target_file == "build" .. getSeparator() .. "changelog".. getSeparator() ..  "index.html")

if isRoot or isRootChangelog then
  doLog("debug", "[DATA BUILDER]: ROOT DETECTED! Paths are..." .. target_file)
  local cleaned_path = Regex.replace_all(project, "-", "_")
  setPathAttr(cleaned_path, 'path', 'true')
  return
end

while dirN <= dirCount do
  local cwd = dirs[dirN]

  doLog('debug', "[DATA BUILDER]: checking workdir: " .. cwd .. " against target file: " .. target_file)

  local skip = (skipDir(cwd)
                or cwd == "build" .. getSeparator() .. "changelog"
                or cwd == "build" .. getSeparator() .. "blog"
                or Sys.is_file(cwd))

  if not skip and not isRoot then
    doLog('debug', "[DATA BUILDER]: Found valid cwd, writing title for... " .. cwd)

    local subPaths = Regex.split(cwd, getSeparator())
    local modName = subPaths[size(subPaths)]

    modName = Regex.replace_all(modName, "-", "_")

    if setPathAttr(modName, 'path') then return end

  end

  dirN = dirN + 1
end

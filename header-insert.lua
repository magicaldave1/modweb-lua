local org = global_data.org
local project = global_data.project
local use_digests = global_data.use_digests

local links_to_correct = {
    'dev-download',
    'changelog',
    'headerHistory',
    'headerIssues',
}

local nFixes = 1
local fixCount = Table.length(links_to_correct)

modData = HTML.select_one(page, "modData")

if not modData then
    Plugin.fail("[HEADER FIXER]: Failed to locate required element in the page! No modData found!")
end

modPath = HTML.get_attribute(modData, "path")

do_log = global_data.do_log
function doLog(level, entry)
  if not do_log or do_log ~= "true" then return end

  if level == "warning" then
    Log.warning(entry)
  elseif level == "error" then
    Log.error(entry)
  elseif level == "debug" then
    Log.debug(entry)
  elseif level == "info" then
    Log.info(entry)
  end
end

function getSeparator()
  if Sys.is_windows() then
    return '\\'
  else
    return '/'
  end
end

local headerTemplate = HTML.parse(Sys.read_file('templates' .. getSeparator() .. 'header.html'))

local links = HTML.select_one(headerTemplate, "#links")

if not links then
    Plugin.fail("[HEADER FIXER]: Unable to find links element!")
end

local downloadButton = HTML.create_element('a', 'Download')

local modHeader = HTML.select_one(page, ".modHeader")

HTML.add_class(downloadButton, "button")
HTML.set_attribute(downloadButton, 'id', "download")
HTML.set_attribute(downloadButton, 'title', "Download the latest release of " .. (HTML.inner_text(modHeader) or modPath))

HTML.append_child(links, downloadButton)

while (nFixes <= fixCount) do
    local idToFix = links_to_correct[nFixes]
    local toFix = HTML.select_one(headerTemplate, "#" .. idToFix)
    local link = HTML.get_attribute(toFix, "href")

    if link ~= nil then

        if idToFix == "changelog" then

            if target_file ~= 'build' .. getSeparator() .. 'changelog' .. getSeparator() .. 'index.html'
                and target_file ~= 'build' .. getSeparator() .. 'index.html' then

                doLog('info', "[HEADER FIXER]: Not a root file, correcting changelog URI in file " .. target_file)

                HTML.set_attribute(toFix, "href", '/' .. modPath .. link)

            end

        else
            local modPath = HTML.get_attribute(modData, "path")

            if not modPath then return end

            local replacedOrg = Regex.replace(link, "ORG", org)
            local replacedProj = Regex.replace(replacedOrg, "PROJECT", project)
            local sanitized_path = Regex.replace_all(modPath, '-', '_')
            local replacedPath = Regex.replace(replacedProj, "MOD", sanitized_path)

            HTML.set_attribute(toFix, "href", replacedPath)
        end
    else
        Plugin.fail("[HEADER FIXER]: Required element was not found in the HTML tree: " .. idToFix)
    end

    nFixes = nFixes + 1
end

local header = HTML.select_one(page, "#downloadHeader")
local dev_build = HTML.select_one(headerTemplate, "#dev-build")

if use_digests and use_digests == 'true' then
  local wrap_begin = HTML.create_text('(')
  local wrap_middle = HTML.create_text(' / ')
  local wrap_end = HTML.create_text(')')

  local sha_256 = '<a id="dev-sha256" href="https://gitlab.com/api/v4/projects/ORG%2FPROJECT/jobs/artifacts/master/raw/MOD.sha256sum.txt?job=make" title="Verify the integrity of the file you downloaded">sha256</a>'
  local sha_512 = '<a id="dev-sha512" href="https://gitlab.com/api/v4/projects/ORG%2FPROJECT/jobs/artifacts/master/raw/MOD.sha512sum.txt?job=make" title="Verify the integrity of the file you downloaded">sha512</a>'

  sha_512 = Regex.replace_all(sha_512, "ORG", org)
  sha_256 = Regex.replace_all(sha_256, "ORG", org)

  sha_256 = Regex.replace_all(sha_256, "PROJECT", project)
  sha_512 = Regex.replace_all(sha_512, "PROJECT", project)

  sha_512 = Regex.replace_all(sha_512, "MOD", HTML.get_attribute(modData, "path"))
  sha_256 = Regex.replace_all(sha_256, "MOD", HTML.get_attribute(modData, "path"))

  HTML.append_child(dev_build, wrap_begin)
  HTML.append_child(dev_build, HTML.parse(sha_256))
  HTML.append_child(dev_build, wrap_middle)
  HTML.append_child(dev_build, HTML.parse(sha_512))
  HTML.append_child(dev_build, wrap_end)
else
  HTML.delete(HTML.select_one(headerTemplate, '#shas'))
  local dev_build = HTML.select_one(headerTemplate, '#dev-build')
  HTML.set_attribute(dev_build, 'style', 'margin-top:0px;')
end

HTML.append_child(header, headerTemplate)

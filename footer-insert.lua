local org = global_data.org
local project = global_data.project
global_exclude_paths = global_data.global_exclude_paths
local extra_include_dirs = config.extra_exclude_dirs

local discord = global_data.discord
local irc = global_data.irc
local email = global_data.email
local issues_page = global_data.issues_page

do_log = global_data.do_log
function doLog(level, entry)
  if not do_log or do_log ~= "true" then return end

  if level == "warning" then
    Log.warning(entry)
  elseif level == "error" then
    Log.error(entry)
  elseif level == "debug" then
    Log.debug(entry)
  elseif level == "info" then
    Log.info(entry)
  end
end

function getSeparator()
  if Sys.is_windows() then
    return '\\'
  else
    return '/'
  end
end

function skipDir(dir)
  local dirSplit = Regex.split(dir, getSeparator())
  local baseDir = dirSplit[Table.length(dirSplit)]
  return Regex.match(baseDir, global_exclude_paths)
end

local no_problem = 'false'

if skipDir(target_file) then
  doLog('debug', "Skipping this file! Globally excluded directory: " .. target_file)
  no_problem = 'true'
  -- return
end

if extra_include_dirs and Regex.match(target_file, extra_include_dirs) then
  doLog('debug', "Skipping this file due to extra include dirs: " .. target_file)
  no_problem = 'true'
  -- return
end

if not discord and not irc and not email and (not issues_page or issues_page ~= 'true') then
  doLog('debug', "No contact info was provided! Omitting contact page.")
  no_problem = 'true'
end

local footerTemplate = HTML.parse(Sys.read_file('templates' .. getSeparator() .. 'footer.html'))
local footer = HTML.select_one(footerTemplate, "#end")
local contact_info = HTML.select_one(footer, '#contactInfo')

if irc then

  local irc_elem = HTML.create_element('code', '@' .. irc)
  local irc_tag = HTML.create_element('p', "Contact the author on IRC: ")

  HTML.append_child(irc_tag, irc_elem)

  HTML.insert_after(contact_info, irc_tag)

end

if discord then

  local discord_elem = HTML.create_element('code', '@' .. discord)
  local discord_tag = HTML.create_element('p', "Contact the author on Discord: ")

  HTML.append_child(discord_tag, discord_elem)

  HTML.insert_after(contact_info, discord_tag)

end

if email then

  local email_elem = HTML.create_element('code', email)
  local email_tag = HTML.create_element('p', "Email: ")

  HTML.append_child(email_tag, email_elem)

  HTML.insert_after(contact_info, email_tag)

end

if issues_page and issues_page == 'true' then

  local paragraph = HTML.create_element('p')
  local link = HTML.create_element('a', "Open an issue on GitLab")
  local template_issues = "https://gitlab.com/ORG/PROJECT/-/issues"

  template_issues = Regex.replace(template_issues, "ORG", org)
  template_issues = Regex.replace(template_issues, "PROJECT", project)

  HTML.set_attribute(link, 'href', template_issues)

  HTML.append_child(paragraph, link)

  HTML.insert_after(contact_info, paragraph)

end

local page_footer = HTML.select_one(page, "footer")

if no_problem and no_problem == 'true' then
  HTML.delete(HTML.select_one(footerTemplate, '#end'))
end

HTML.append_child(page_footer, footerTemplate)
